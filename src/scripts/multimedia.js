/* Despliego y repliego la caja de texto debajo del video principal */
var verMas = function() {
    var textBlock = document.getElementById("js-text-content");
    var textBlockHandler = document.getElementById("js-abrir-text-content");
    textBlock.classList.toggle("open");
    textBlockHandler.classList.toggle("cerrar");
    var textHandler = textBlockHandler.textContent;
    if(textHandler == 'Mostrar más') {
        textBlockHandler.textContent = "Mostrar menos";
    } else {
        textBlockHandler.textContent = "Mostrar más";
    }

}
document.getElementById("js-abrir-text-content").addEventListener("click",verMas,false);

/****************************************** */


/* Inserto el video en el iframe y el titulo y texto debajo haciendo click en el thumbnail */
function loadDoc(xy) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var xmlDoc = this.responseXML;
            var x = xmlDoc.getElementsByTagName("video");
            var numero = xy;
            var insertarText = '<h3>' + 
                        x[numero].getElementsByTagName("title")[0].childNodes[0].nodeValue + 
                        '</h3>' + 
                        x[numero].getElementsByTagName("text")[0].childNodes[0].nodeValue;
            var insertarVideo = '<iframe id="vrudo" class="video-embed__vrudo" title="' + 
            x[numero].getElementsByTagName("title")[0].childNodes[0].nodeValue + 
            '" src="' + x[numero].getElementsByTagName("src")[0].childNodes[0].nodeValue + ' " width="100%" height="100%" allowscriptaccess="always" allowfullscreen="true" webkitallowfullscreen="true" frameborder="0" scrolling="no" allow="autoplay; fullscreen" loading="lazy"></iframe>';
            document.getElementById("js-video-text-content").innerHTML = insertarText;
            document.getElementById("js-videoentrevista").innerHTML = insertarVideo;

        }
    };
    xhttp.open("GET", "../assets/data/videos.xml", true);
    xhttp.send();
}


/* Al cargar el body arma la lista de thumbnails de video con foto titulo y link */
function cargar() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            listado(this);
        }
    };
    xhttp.open("GET", "../assets/data/videos.xml", true);
    xhttp.send();
}

function listado(xml) {
    var i;
    var xmlDoc = xml.responseXML;
    var title = xmlDoc.getElementsByTagName("title");
    var fotito = xmlDoc.getElementsByTagName("foto");
    var source = xmlDoc.getElementsByTagName("src");
    var columna = "";
    for (i = 0; i < title.length; i++) {
        columna += '<li><button onclick="loadDoc(' + i + ')">' + title[i].childNodes[0].nodeValue + '</button><img src="./assets/fotos/videos/previews/' + fotito[i].childNodes[0].nodeValue + '" alt="' + title[i].childNodes[0].nodeValue + '"><span>' + title[i].childNodes[0].nodeValue + '</span></li>'; 
    }
    document.getElementById("js-video-lista").innerHTML = columna;
}

Window.onload = cargar();