function start() {
	loadFotos(0);
	loadListado();
}

function loadFotos(a) {
	// armo la primera fotogaleria
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            listaFotos(this, a);
        }
    };
    xhttp.open("GET", "../assets/data/fotoslider-" + a + ".xml", true);
    xhttp.send();
}

function loadListado() {
	// armo el listado de fotogalerias 
    var xhttp2 = new XMLHttpRequest();
    xhttp2.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            listaSliders(this);
        }
    };
    xhttp2.open("GET", "../assets/data/sliders.xml?v=1.0.7", true);
    xhttp2.send();
}

 
function listaFotos(xml, a) {
    var i;
    var xmlDoc = xml.responseXML;
	/* defino las variables para cada subnodo del nodo '<foto>'  */
    var imagen = xmlDoc.getElementsByTagName("imagen");
    var caption = xmlDoc.getElementsByTagName("caption");
    var credito = xmlDoc.getElementsByTagName("credito");
    var unidad = ""; // defino una variable vacía y la completo en el for loop.

	/* Con el for loop traigo los datos de cada nodo '<foto>' */
    for (i = 0; i < imagen.length; i++) {
        unidad += '<figure class="fotoslider__contenedor__slider__section"><picture><source media="(max-width: 727px)" srcset="assets/fotos/fotogaleria/slider-' + a + '/520/' + imagen[i].childNodes[0].nodeValue + '"><img src="assets/fotos/fotogaleria/slider-' + a + '/' + imagen[i].childNodes[0].nodeValue + '" alt="' + caption[i].childNodes[0].nodeValue + ' "loading="lazy"></picture><input type="checkbox" class="togCheck" id="check-' + i + '"/><figcaption><label for="check-' + i + '" class="togButton"></label>' + caption[i].childNodes[0].nodeValue + ' | ' + credito[i].childNodes[0].nodeValue + '</figcaption></figure>'; 
    }

	/* 
	Las fotos deben ser relación 16:9. Como no sé cuántas fotos hay,
	el padding debe ser 56.25 dividido por el total de fotos. 
	El width total del slider debe ser el total de fotos multiplicado por cien (en porcentaje). 
	Ejemplo: 6 fotos, width 600% - 10 fotos, width 1000 % (porque cada foto es 100%). 
	*/
    var todos = i * 100;
    var paddingbottom = 56.25 / i;    
	var jsSlider = document.getElementById("js-slider");
    jsSlider.innerHTML = unidad;  // inserto las fotos y sus datos en el div js-slider
	jsSlider.setAttribute("title", a);
    jsSlider.style.width = todos + "%";  // le doy el width al div js-slider
    Array.prototype.forEach.call(document.getElementsByClassName('fotoslider__contenedor__slider__section'), 
                             item => item.style.paddingBottom = paddingbottom + '%');  // le doy el valor al padding bottom a cada figure
	document.getElementById("js-titulo").innerHTML = xmlDoc.getElementsByTagName("titulo")[0].childNodes[0].nodeValue;
	

	/****************************************
	 ****************************************
	 **************************************** 
 
	 A partir de acá las funciones para 
	 el auto slide y los botones left y right 
	 para pasar fotos. Los estilos están
	 en un .css

	 **************************************** 
	 **************************************** 
	 ****************************************/

	var sliderSection = document.querySelectorAll(".fotoslider__contenedor__slider__section");
	var sliderSectionLast = sliderSection[sliderSection.length -1];
	var btnLeft = document.getElementById("fotoslider-btn-left");
	var btnRight = document.getElementById("fotoslider-btn-right");

	jsSlider.insertAdjacentElement('afterbegin', sliderSectionLast);

	function Next() {
		let sliderSectionFirst = document.querySelectorAll(".fotoslider__contenedor__slider__section")[0];
		jsSlider.style.marginLeft = "-200%";
		jsSlider.style.transition = "ease .5s";
		setTimeout(function(){
			jsSlider.style.transition = "none";
			jsSlider.insertAdjacentElement('beforeend', sliderSectionFirst);
			jsSlider.style.marginLeft = "-100%";
		}, 500);
	}
	btnRight.addEventListener('click', function(){
		Next();
	}, {passive: true});

	function Prev() {
		var sliderSection = document.querySelectorAll(".fotoslider__contenedor__slider__section");
		var sliderSectionLast = sliderSection[sliderSection.length -1];
		jsSlider.style.marginLeft = "0";
		jsSlider.style.transition = "ease .5s";
		setTimeout(function(){
			jsSlider.style.transition = "none";
			jsSlider.insertAdjacentElement('afterbegin', sliderSectionLast);
			jsSlider.style.marginLeft = "-100%";
		}, 500);
	}

	btnLeft.addEventListener('click', function(){
		Prev();
	}, {passive: true});

/**
 * Este es el autoscroll. Anda medio raro. Si jode lo sacaría y listo.

	setInterval(function(){
		Next();
	}, 15000);
 */
}


/**
 * 
 * funcion para llamar y cargar el xml que tiene la lista de sliders. 
 */
function listaSliders(xml) {
    var j;
    var xmlDoc = xml.responseXML;
    var tema = xmlDoc.getElementsByTagName("tema");
    var todos = "";
    for (j = 0; j < tema.length; j++) {
        todos += '<li class="menu-sliders"><button onclick="loadFotos(' + j + ')" class="menu-sliders__boton" >' + tema[j].childNodes[0].nodeValue + '</button><figure><img src="assets/fotos/fotogaleria/slider-' + j + '/thumb.jpg" alt="slider-' + j + '" width="100"><figcaption>' + tema[j].childNodes[0].nodeValue + '</figcaption></figure>'; 
    }
	document.getElementById("js-lista-sliders").innerHTML = todos;
}



/*********************************************************
 * *******************************************************
 * *******************************************************
 * 
 * funciones para los manejadores del listado de galerias.
 * 
 * *******************************************************
 * *******************************************************
 *********************************************************/

let listadoSliders = document.getElementById("js-lista-sliders");
let botonRight = document.getElementById("js-fotoslider-boton-right")
let botonLeft = document.getElementById("js-fotoslider-boton-left")
function adelante() { //el boton que desplaza el contenido del listado hacia el final
	listadoSliders.scrollBy({
		top: 0,
		left: 250,
		behavior: "smooth"}
	);

}
function atras() { //el boton que desplaza el contenido del listado hacia el inicio
	listadoSliders.scrollBy({
		top: 0,
		left: -250,
		behavior: "smooth"}
	);
}

function scrolled() { //asigna display:none o display:flex a los botones segun sea necesario que se vean o no 
	if(listadoSliders.offsetWidth + listadoSliders.scrollLeft == listadoSliders.scrollWidth) {
		botonRight.style.display = "none";
	} else {
		botonRight.style.display = "flex"
	}
	if(listadoSliders.scrollLeft == 0) {
		botonLeft.style.display = "none"
	} else {
		botonLeft.style.display = "flex"
	}
}


// on resize, cambia la logica del slider de swipe a botones cuando el viewport es mayor a 960px
const layoutChangedCallback = (matches) => {
	if (matches) {
		var jsSlider = document.getElementById("js-slider");
		jsSlider.innerHTML = "";
		jsSlider.style.width = "100%"
		setTimeout(() => {
			var galeria = document.getElementById("js-slider").getAttribute("title");
			loadFotos(galeria);
		}, 200);
	}
	// console.log(matches ? "I'm on desktop!" : "I'm on mobile!");
}
	
// defino en que media query voy a hacer el cambio
const mql = window.matchMedia('(min-width: 960px)');

// set listener to run callback
mql.addEventListener('change', (e) => layoutChangedCallback(e.matches), {passive: true});

// the callback doesn't run immediately, so we run it manually once
layoutChangedCallback(mql.matches);



// llamo a la funcion principal que arma la primera fotogaleria y el listado de todas.
Window.onload = start();
