var menu = function() { 
    var mainnav = document.getElementById("main-nav");
    var handler = document.getElementById("main-nav-handler"); 
    mainnav.classList.toggle("mostrar");
    handler.classList.toggle("cerrar");
}
document.getElementById("main-nav-handler").addEventListener("click",menu,false);



//Funcion para reconocer el scroll de la pagina y modificar las clases del header.
var lastScrollTop = 0;

window.addEventListener("scroll", function(){ 
    var jsHeader = document.getElementById("js-header");
    var jsMainHeader = document.getElementById("js-main-header");
    var jsMainContainer = document.getElementById("js-main-container");
    var st = window.pageYOffset; 
    if (st > lastScrollTop && st > 50){
        jsHeader.classList.add("minified");
    } else {
        jsHeader.classList.remove("minified");
    }
    lastScrollTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling
    if (this.window.pageYOffset > 0) {
        jsMainHeader.classList.add("scroled-down");
        this.document.getElementById("js-main-container").classList.add("scroled-down");
    }  else  {
        jsMainHeader.classList.remove("scroled-down");
        jsMainContainer.classList.remove("scroled-down");
    }
}, false);