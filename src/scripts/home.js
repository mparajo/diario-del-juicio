/* Despliego y repliego la caja de texto debajo del video principal */
var verTodo = function() {
    var lastParagraph = document.getElementById("js-last-paragraph");
    var abrirMmTextContent = document.getElementById("js-abrir-mm-text-content");
    lastParagraph.classList.toggle("open");
    abrirMmTextContent.classList.toggle("cerrar");
    var textHandler = abrirMmTextContent.textContent;
    if(textHandler == 'Mostrar más') {
        abrirMmTextContent.textContent = "Mostrar menos";
    } else {
        abrirMmTextContent.textContent = "Mostrar más";
    }
}
document.getElementById("js-abrir-mm-text-content").addEventListener("click",verTodo,false);