function loadTodo() {
	loadDiario();
	loadPdf();
}



function loadDiario() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      notasRssPerfil(this);
    }
  };
  xhttp.open("GET", "https://www.perfil.com/feed/juicio-a-las-juntas-militares", true);
  xhttp.send();
}

function loadPdf() {
  var xhttp2 = new XMLHttpRequest();
  xhttp2.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      notasPdf(this);
    }
  };
  xhttp2.open("GET", "../assets/data/notas-pdf.xml?v=1.0.7", true);
  xhttp2.send();
}

function notasRssPerfil(xml) {
    var i;
    var xmlDoc = xml.responseXML;
    var contenido = "";
    var x = xmlDoc.getElementsByTagName("item");
    for (i = 0; i <x.length; i++) { 
        var media = x[i].getElementsByTagName('media:content')[0];
        var src = media.getAttribute("url");
        contenido += '<article class="nota-perfil"><a href="' +  x[i].getElementsByTagName("link")[0].childNodes[0].nodeValue  + '" rel="noreferrer" target="_blank"><figure><img src="' +  src  + '" alt="' + x[i].getElementsByTagName("title")[0].childNodes[0].nodeValue + '" loading="lazy" /></figure><h3>' + x[i].getElementsByTagName("title")[0].childNodes[0].nodeValue + '</h3></a></article>';
    }
        
    document.getElementById("js-contenido-notas").innerHTML = contenido;
}


function notasPdf(xml) {
  var i;
  var xmlDoc = xml.responseXML;
  var p="";
  var y = xmlDoc.getElementsByTagName("diario");
  for (i = 0; i <y.length; i++) { 
    p += '<article class="nota-pdf"><a href="' +  y[i].getElementsByTagName("link")[0].childNodes[0].nodeValue  + '" rel="noreferrer" target="_blank"><figure><img src="/assets/fotos/diario/diario-del-juicio-' +  y[i].getElementsByTagName("foto")[0].childNodes[0].nodeValue  + '.jpg" alt="Diario del Juicio - ' +  y[i].getElementsByTagName("edicion")[0].childNodes[0].nodeValue  + ' loading="lazy"></figure><div class="nota-pdf__data"><h4 class="nota-pdf__data__edicion">' +  y[i].getElementsByTagName("edicion")[0].childNodes[0].nodeValue  + '</h4><div class="nota-pdf__data__fecha">' +  y[i].getElementsByTagName("fecha")[0].childNodes[0].nodeValue  + '</div><div class="nota-pdf__data__temas">' +  y[i].getElementsByTagName("temas")[0].childNodes[0].nodeValue  + '</div><div class="nota-pdf__data__descargar">Descargar PDF</div></div></a></article>';
  }

  document.getElementById("js-contenido-notas-pdf").innerHTML = p;

}


Window.onload = loadTodo();